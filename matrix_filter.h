#pragma once

#include "filters.h"

struct Neighbor {
    int32_t dx = 0;
    int32_t dy = 0;
    double coef = 0;
};

class MatrixFilter : public Filter {
public:
    MatrixFilter(const std::vector<Neighbor> &arr);

    Image Apply(const Image &img);

    std::vector<Neighbor> matrix_;
};
