#include "gaussian_blur.h"
#include <cmath>

GaussianBlur::GaussianBlur(const int32_t sigma) {
    sigma_ = sigma;
}

Image GaussianBlur::Apply(const Image &img) {
    int32_t width = img.GetWidth();
    int32_t height = img.GetHeight();
    Image ans(width, height);
    std::vector<Neighbor> arr;
    double divider = 0;
    for (int32_t dx = -3 * sigma_; dx <= 3 * sigma_; ++dx) {
        for (int32_t dy = -3 * sigma_; dy <= 3 * sigma_; ++dy) {
            double multiplier = 1 / (2 * std::numbers::pi * sigma_ * sigma_) *
                                pow(std::numbers::e,
                                    -((static_cast<double>(dy * dy + dx * dx)) /
                                      (static_cast<double>(2 * sigma_ * sigma_))));
            arr.push_back(Neighbor{dx, dy, multiplier});
            divider += multiplier;
        }
    }
    for (auto &elem : arr) {
        elem.coef /= divider;
    }
    auto gaussian_matrix = std::make_unique<MatrixFilter>(arr);
    return gaussian_matrix->Apply(img);
}
