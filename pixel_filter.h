#pragma once

#include "filters.h"

class PixelFilter : public Filter {
public:
    Image Apply(const Image &img);
};
