#pragma once

#include "filters.h"

class GrayScale : public Filter {
public:
    Image Apply(const Image &img);
};
