#include "parser.h"

void Parser::Parser(Image &img, int argc, const char *argv[]) {
    for (int i = 3; i < argc; ++i) {
        std::unique_ptr<Filter> f;
        std::string current_filter = std::string(argv[i]);
        if (current_filter == "-crop") {
            if (i + 2 >= argc) {
                throw std::invalid_argument("incorrect arguments in crop filter");
            }
            try {
                int32_t width = std::stoi(argv[i + 1]);
                int32_t height = std::stoi(argv[i + 2]);
                if ((std::stof(argv[i + 1]) - std::stoi(argv[i + 1]) != 0.0) || (std::stof(argv[i + 2]) - std::stoi(argv[i + 2]) != 0.0) ||
                    (std::stof(argv[i + 1]) <= 0) || (std::stof(argv[i + 2]) <= 0)) {
                    throw std::invalid_argument("incorrect arguments in crop filter");
                }
                f = std::make_unique<Crop>(width, height);
                i += 2;
            } catch (...) {
                throw std::invalid_argument("incorrect arguments in crop filter");
            }
        } else if (current_filter == "-gs") {
            f = std::make_unique<GrayScale>();
        } else if (current_filter == "-neg") {
            f = std::make_unique<Negative>();
        } else if (current_filter == "-sharp") {
            f = std::make_unique<Sharpening>();
        } else if (current_filter == "-edge") {
            if (i + 1 == argc) {
                throw std::invalid_argument("invalid arguments in edge detection filter");
            }
            try {
                if (std::stof(argv[i + 1]) < 0) {
                    throw std::invalid_argument("invalid arguments in edge detection filter");
                }
                int32_t threshold = std::stof(argv[i + 1]) * 255;
                f = std::make_unique<EdgeDetectionFilter>(threshold);
                ++i;
            } catch (...) {
                throw std::invalid_argument("invalid arguments in edge detection filter");
            }
        } else if (current_filter == "-blur") {
            if (i + 1 == argc) {
                throw std::invalid_argument("invalid argument in gaussian blur filter");
            }
            try {
                if (std::stof(argv[i + 1]) <= 0) {
                    throw std::invalid_argument("invalid argument in gaussian blur filter");
                }
                int32_t sigma = std::stof(argv[i + 1]) + 0.5;
                f = std::make_unique<GaussianBlur>(sigma);
                ++i;
            } catch (...) {
                throw std::invalid_argument("invalid argument in gaussian blur filter");
            }
        } else if (current_filter == "-pixel") {
            f = std::make_unique<PixelFilter>();
        } else {
            throw std::invalid_argument("undefined filter");
        }
        img = f->Apply(img);
    }
}