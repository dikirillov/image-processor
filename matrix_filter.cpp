#include "matrix_filter.h"

MatrixFilter::MatrixFilter(const std::vector<Neighbor> &arr) {
    matrix_ = arr;
}

Image MatrixFilter::Apply(const Image &img) {
    int32_t width = img.GetWidth();
    int32_t height = img.GetHeight();
    Image ans(width, height);
    for (int32_t i = 0; i < height; ++i) {
        for (int32_t j = 0; j < width; ++j) {
            double current_red = 0;
            double current_green = 0;
            double current_blue = 0;
            for (const auto &elem: matrix_) {
                int32_t y = std::min(height - 1, std::max(0, i + elem.dy));
                int32_t x = std::min(width - 1, std::max(0, j + elem.dx));
                current_red += img.GetOnePixel(x, y).red * elem.coef;
                current_green += img.GetOnePixel(x, y).green * elem.coef;
                current_blue += img.GetOnePixel(x, y).blue * elem.coef;
            }
            Pixel ans_pixel;
            ans_pixel.red = std::max(0.0, std::min(255.0, current_red));
            ans_pixel.green = std::max(0.0, std::min(255.0, current_green));
            ans_pixel.blue = std::max(0.0, std::min(255.0, current_blue));
            ans.WriteOnePixel(j, i, ans_pixel);
        }
    }
    return ans;
}