#include "image.h"

Image::Image(int32_t width, int32_t height) {
    width_ = width;
    height_ = height;
    data_ = std::vector<std::vector<Pixel>>{static_cast<size_t>(height), std::vector<Pixel>(width)};
}

void Image::WriteOnePixel(const int32_t y, const int32_t x, Pixel pix) {
    data_[x][y] = std::move(pix);
}

Pixel Image::GetOnePixel(const int32_t y, const int32_t x) const {
    return data_[x][y];
}

int32_t Image::GetWidth() const {
    return width_;
}

int32_t Image::GetHeight() const {
    return height_;
}

Image &Image::operator=(const Image &other) {
    width_ = other.width_;
    height_ = other.height_;
    data_ = other.data_;
    return *this;
}

bool Image::operator==(const Image &other) const {
    if (width_ != other.width_ || height_ != other.height_) {
        return false;
    }
    for (int32_t x = 0; x < height_; ++x) {
        for (int32_t y = 0; y < width_; ++y) {
            Pixel current_pix_1 = this->GetOnePixel(y, x);
            Pixel current_pix_2 = other.GetOnePixel(y, x);
            if (current_pix_1.red != current_pix_2.red || current_pix_1.green != current_pix_2.green ||
                current_pix_1.blue != current_pix_2.blue) {
                return false;
            }
        }
    }
    return true;
}
