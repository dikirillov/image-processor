#pragma once

#include "matrix_filter.h"
#include "gray_scale.h"

class EdgeDetectionFilter : public Filter {
public:
    EdgeDetectionFilter(const int32_t threshold);

    Image Apply(const Image &img);

private:
    int32_t threshold_;
};