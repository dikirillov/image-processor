#include "pixel_filter.h"

Image PixelFilter::Apply(const Image &img) {
    int32_t width = img.GetWidth();
    int32_t height = img.GetHeight();
    Image ans(width, height);
    std::vector<int32_t> dx = {0};
    std::vector<int32_t> dy = {0};
    int32_t size_of_pixel = 15;
    for (int32_t i = 1; i <= size_of_pixel; ++i) {
        dx.push_back(i);
        dx.push_back(-i);
        dy.push_back(i);
        dy.push_back(-i);
    }
    for (int32_t y = 0; y < height + size_of_pixel; y += 2 * size_of_pixel + 1) {
        for (int32_t x = 0; x < width + 20; x += 2 * size_of_pixel + 1) {
            int red = 0;
            int green = 0;
            int blue = 0;
            for (const auto move_x: dx) {
                for (const auto move_y: dy) {
                    int32_t new_y = std::max(0, std::min(height - 1, y + move_y));
                    int32_t new_x = std::max(0, std::min(width - 1, x + move_x));
                    Pixel current_pixel = img.GetOnePixel(new_x, new_y);
                    red += current_pixel.red;
                    green += current_pixel.green;
                    blue += current_pixel.blue;
                }
            }
            Pixel ans_pix;
            ans_pix.red = std::max(0, std::min(255, red / ((2 * size_of_pixel + 1) * (2 * size_of_pixel + 1))));
            ans_pix.green = std::max(0, std::min(255, green / ((2 * size_of_pixel + 1) * (2 * size_of_pixel + 1))));
            ans_pix.blue = std::max(0, std::min(255, blue / ((2 * size_of_pixel + 1) * (2 * size_of_pixel + 1))));
            for (const auto move_x: dx) {
                for (const auto move_y: dy) {
                    int32_t new_x = std::max(0, std::min(height - 1, y + move_y));
                    int32_t new_y = std::max(0, std::min(width - 1, x + move_x));
                    ans.WriteOnePixel(new_y, new_x, ans_pix);
                }
            }
        }
    }
    return ans;
}
