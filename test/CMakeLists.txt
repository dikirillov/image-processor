add_executable(tests
        all_tests.cpp
        ../crop.cpp
        ../gray_scale.cpp
        ../image.cpp
        ../matrix_filter.cpp
        ../negative.cpp
        ../sharpening.cpp
        ../edge_detection.cpp
        ../bmp.cpp
        ../image.cpp
        ../gaussian_blur.cpp
        ../parser.cpp
        ../pixel_filter.cpp
        )

target_link_libraries(tests contrib_catch_main)