#include <catch.hpp>
#include "../crop.h"
#include "../gray_scale.h"
#include "../negative.h"
#include "../sharpening.h"
#include "../parser.h"

TEST_CASE("GrayScale") {
    Image img(1, 1);
    img.WriteOnePixel(0, 0, Pixel{50, 100, 20});
    Image ans(1, 1);
    ans.WriteOnePixel(0, 0, Pixel{75, 75, 75});
    GrayScale f;
    REQUIRE(f.Apply(img) == ans);
}

TEST_CASE("Negative") {
    Image img(1, 1);
    img.WriteOnePixel(0, 0, Pixel{50, 100, 20});
    Image ans(1, 1);
    ans.WriteOnePixel(0, 0, Pixel{205, 155, 235});
    Negative f;
    REQUIRE(f.Apply(img) == ans);
}

TEST_CASE("Crop") {
    Image img(10, 10);
    Crop f1(2, 2);
    Crop f2(4, 1);
    Crop f3(1, 4);
    REQUIRE(f1.Apply(img) == Image(2, 2));
    REQUIRE(f2.Apply(img) == Image(4, 1));
    REQUIRE(f3.Apply(img) == Image(1, 4));
}

TEST_CASE("Sharpening") {
    Image img(3, 3);
    img.WriteOnePixel(0, 0, Pixel{30, 40, 50});
    img.WriteOnePixel(0, 1, Pixel{20, 20, 20});
    img.WriteOnePixel(0, 2, Pixel{10, 10, 10});
    img.WriteOnePixel(1, 0, Pixel{20, 70, 100});
    img.WriteOnePixel(1, 1, Pixel{80, 10, 0});
    img.WriteOnePixel(1, 2, Pixel{10, 20, 30});
    img.WriteOnePixel(2, 0, Pixel{20, 0, 20});
    img.WriteOnePixel(2, 1, Pixel{50, 40, 30});
    img.WriteOnePixel(2, 2, Pixel{10, 10, 10});

    Image ans(3, 3);
    ans.WriteOnePixel(0, 0, Pixel{50, 30, 30});
    ans.WriteOnePixel(0, 1, Pixel{0, 20, 20});
    ans.WriteOnePixel(0, 2, Pixel{0, 0, 0});
    ans.WriteOnePixel(1, 0, Pixel{0, 230, 255});
    ans.WriteOnePixel(1, 1, Pixel{255, 0, 0});
    ans.WriteOnePixel(1, 2, Pixel{0, 50, 100});
    ans.WriteOnePixel(2, 0, Pixel{0, 0, 0});
    ans.WriteOnePixel(2, 1, Pixel{90, 140, 90});
    ans.WriteOnePixel(2, 2, Pixel{0, 0, 0});
    Sharpening f;
    REQUIRE(f.Apply(img) == ans);
}

TEST_CASE("Edge detection") {
    EdgeDetectionFilter f(10);
    Image img1(3, 3);
    img1.WriteOnePixel(0, 0, Pixel{10, 10, 10});
    img1.WriteOnePixel(0, 1, Pixel{100, 0, 0});
    img1.WriteOnePixel(0, 2, Pixel{10, 10, 10});
    img1.WriteOnePixel(1, 0, Pixel{10, 10, 10});
    img1.WriteOnePixel(1, 1, Pixel{10, 10, 10});
    img1.WriteOnePixel(1, 2, Pixel{100, 0, 0});
    img1.WriteOnePixel(2, 0, Pixel{10, 10, 10});
    img1.WriteOnePixel(2, 1, Pixel{10, 10, 10});
    img1.WriteOnePixel(2, 2, Pixel{10, 10, 10});

    Image ans1(3, 3);
    ans1.WriteOnePixel(0, 0, Pixel{0, 0, 0});
    ans1.WriteOnePixel(0, 1, Pixel{255, 255, 255});
    ans1.WriteOnePixel(0, 2, Pixel{0, 0, 0});
    ans1.WriteOnePixel(1, 0, Pixel{0, 0, 0});
    ans1.WriteOnePixel(1, 1, Pixel{0, 0, 0});
    ans1.WriteOnePixel(1, 2, Pixel{255, 255, 255});
    ans1.WriteOnePixel(2, 0, Pixel{0, 0, 0});
    ans1.WriteOnePixel(2, 1, Pixel{0, 0, 0});
    ans1.WriteOnePixel(2, 2, Pixel{0, 0, 0});
    REQUIRE(f.Apply(img1) == ans1);

    Image img2(3, 3);
    img2.WriteOnePixel(0, 0, Pixel{10, 10, 10});
    img2.WriteOnePixel(0, 1, Pixel{10, 10, 10});
    img2.WriteOnePixel(0, 2, Pixel{10, 10, 10});
    img2.WriteOnePixel(1, 0, Pixel{10, 10, 10});
    img2.WriteOnePixel(1, 1, Pixel{10, 10, 10});
    img2.WriteOnePixel(1, 2, Pixel{10, 10, 10});
    img2.WriteOnePixel(2, 0, Pixel{10, 10, 10});
    img2.WriteOnePixel(2, 1, Pixel{10, 10, 10});
    img2.WriteOnePixel(2, 2, Pixel{10, 10, 10});

    Image ans2(3, 3);
    ans2.WriteOnePixel(0, 0, Pixel{0, 0, 0});
    ans2.WriteOnePixel(0, 1, Pixel{0, 0, 0});
    ans2.WriteOnePixel(0, 2, Pixel{0, 0, 0});
    ans2.WriteOnePixel(1, 0, Pixel{0, 0, 0});
    ans2.WriteOnePixel(1, 1, Pixel{0, 0, 0});
    ans2.WriteOnePixel(1, 2, Pixel{0, 0, 0});
    ans2.WriteOnePixel(2, 0, Pixel{0, 0, 0});
    ans2.WriteOnePixel(2, 1, Pixel{0, 0, 0});
    ans2.WriteOnePixel(2, 2, Pixel{0, 0, 0});
    REQUIRE(f.Apply(img2) == ans2);

    Image img3(3, 3);
    img3.WriteOnePixel(0, 0, Pixel{10, 10, 10});
    img3.WriteOnePixel(0, 1, Pixel{100, 0, 0});
    img3.WriteOnePixel(0, 2, Pixel{10, 10, 10});
    img3.WriteOnePixel(1, 0, Pixel{100, 0, 0});
    img3.WriteOnePixel(1, 1, Pixel{10, 10, 10});
    img3.WriteOnePixel(1, 2, Pixel{100, 0, 0});
    img3.WriteOnePixel(2, 0, Pixel{10, 10, 10});
    img3.WriteOnePixel(2, 1, Pixel{100, 0, 0});
    img3.WriteOnePixel(2, 2, Pixel{10, 10, 10});

    Image ans3(3, 3);
    ans3.WriteOnePixel(0, 0, Pixel{0, 0, 0});
    ans3.WriteOnePixel(0, 1, Pixel{255, 255, 255});
    ans3.WriteOnePixel(0, 2, Pixel{0, 0, 0});
    ans3.WriteOnePixel(1, 0, Pixel{255, 255, 255});
    ans3.WriteOnePixel(1, 1, Pixel{0, 0, 0});
    ans3.WriteOnePixel(1, 2, Pixel{255, 255, 255});
    ans3.WriteOnePixel(2, 0, Pixel{0, 0, 0});
    ans3.WriteOnePixel(2, 1, Pixel{255, 255, 255});
    ans3.WriteOnePixel(2, 2, Pixel{0, 0, 0});
    REQUIRE(f.Apply(img3) == ans3);
}

TEST_CASE("Pixel filter") {
    PixelFilter f;
    Image img1(3, 3);
    img1.WriteOnePixel(0, 0, Pixel{10, 10, 40});
    img1.WriteOnePixel(0, 1, Pixel{50, 25, 30});
    img1.WriteOnePixel(0, 2, Pixel{100, 50, 10});
    img1.WriteOnePixel(1, 0, Pixel{40, 20, 10});
    img1.WriteOnePixel(1, 1, Pixel{100, 50, 50});
    img1.WriteOnePixel(1, 2, Pixel{60, 30, 40});
    img1.WriteOnePixel(2, 0, Pixel{0, 0, 40});
    img1.WriteOnePixel(2, 1, Pixel{50, 25, 20});
    img1.WriteOnePixel(2, 2, Pixel{90, 40, 10});

    Image ans1(3, 3);
    ans1.WriteOnePixel(0, 0, Pixel{47, 24, 25});
    ans1.WriteOnePixel(0, 1, Pixel{47, 24, 25});
    ans1.WriteOnePixel(0, 2, Pixel{47, 24, 25});
    ans1.WriteOnePixel(1, 0, Pixel{47, 24, 25});
    ans1.WriteOnePixel(1, 1, Pixel{47, 24, 25});
    ans1.WriteOnePixel(1, 2, Pixel{47, 24, 25});
    ans1.WriteOnePixel(2, 0, Pixel{47, 24, 25});
    ans1.WriteOnePixel(2, 1, Pixel{47, 24, 25});
    ans1.WriteOnePixel(2, 2, Pixel{47, 24, 25});
    REQUIRE(f.Apply(img1) == ans1);

    Image img2(3, 3);
    img2.WriteOnePixel(0, 0, Pixel{10, 10, 10});
    img2.WriteOnePixel(0, 1, Pixel{10, 10, 10});
    img2.WriteOnePixel(0, 2, Pixel{10, 10, 10});
    img2.WriteOnePixel(1, 0, Pixel{10, 10, 10});
    img2.WriteOnePixel(1, 1, Pixel{10, 10, 10});
    img2.WriteOnePixel(1, 2, Pixel{10, 10, 10});
    img2.WriteOnePixel(2, 0, Pixel{10, 10, 10});
    img2.WriteOnePixel(2, 1, Pixel{10, 10, 10});
    img2.WriteOnePixel(2, 2, Pixel{10, 10, 10});

    Image ans2(3, 3);
    ans2.WriteOnePixel(0, 0, Pixel{10, 10, 10});
    ans2.WriteOnePixel(0, 1, Pixel{10, 10, 10});
    ans2.WriteOnePixel(0, 2, Pixel{10, 10, 10});
    ans2.WriteOnePixel(1, 0, Pixel{10, 10, 10});
    ans2.WriteOnePixel(1, 1, Pixel{10, 10, 10});
    ans2.WriteOnePixel(1, 2, Pixel{10, 10, 10});
    ans2.WriteOnePixel(2, 0, Pixel{10, 10, 10});
    ans2.WriteOnePixel(2, 1, Pixel{10, 10, 10});
    ans2.WriteOnePixel(2, 2, Pixel{10, 10, 10});
    REQUIRE(f.Apply(img2) == ans2);
}

TEST_CASE("Input filters") {
    Image img(1, 1);
    REQUIRE_NOTHROW(Parser::Parser(img, 0, {}));

    const char *argv0[6] = {
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/cmake-build-debug/image_processor",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/example.bmp",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/output.bmp", "-crop", "123", "200"};
    REQUIRE_NOTHROW(Parser::Parser(img, 6, argv0));

    const char *argv6[6] = {
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/cmake-build-debug/image_processor",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/example.bmp",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/output.bmp", "-neg", "-gs", "-pixel"};
    REQUIRE_NOTHROW(Parser::Parser(img, 6, argv6));

    const char *argv8[4] = {
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/cmake-build-debug/image_processor",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/example.bmp",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/output.bmp", "-gs"};
    REQUIRE_NOTHROW(Parser::Parser(img, 4, argv8));

    const char *argv9[4] = {
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/cmake-build-debug/image_processor",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/example.bmp",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/output.bmp", "-neg"};
    REQUIRE_NOTHROW(Parser::Parser(img, 4, argv9));

    const char *argv10[4] = {
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/cmake-build-debug/image_processor",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/example.bmp",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/output.bmp", "-sharp"};
    REQUIRE_NOTHROW(Parser::Parser(img, 4, argv10));

    const char *argv1[4] = {
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/cmake-build-debug/image_processor",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/example.bmp",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/output.bmp", "undefined_filter"};
    REQUIRE_THROWS(Parser::Parser(img, 4, argv1));

    const char *argv2[5] = {
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/cmake-build-debug/image_processor",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/example.bmp",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/output.bmp", "-crop", "123"};
    REQUIRE_THROWS(Parser::Parser(img, 5, argv2));

    const char *argv3[5] = {
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/cmake-build-debug/image_processor",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/example.bmp",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/output.bmp", "-edge", "-100"};
    REQUIRE_THROWS(Parser::Parser(img, 5, argv3));

    const char *argv4[5] = {
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/cmake-build-debug/image_processor",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/example.bmp",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/output.bmp", "-blur", "text"};
    REQUIRE_THROWS(Parser::Parser(img, 5, argv4));

    const char *argv5[5] = {
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/cmake-build-debug/image_processor",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/example.bmp",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/output.bmp", "-edge", "text"};
    REQUIRE_THROWS(Parser::Parser(img, 5, argv5));

    const char *argv7[5] = {
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/cmake-build-debug/image_processor",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/example.bmp",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/output.bmp", "-blur", "0"};
    REQUIRE_THROWS(Parser::Parser(img, 5, argv7));

    const char *argv11[5] = {
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/cmake-build-debug/image_processor",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/example.bmp",
            "/home/dikirillov/cpp-base-hse-2022/projects/image_processor/examples/output.bmp", "-edge", "-gs"};
    REQUIRE_THROWS(Parser::Parser(img, 5, argv11));
}

TEST_CASE("Input and Output files") {
    std::ifstream input1("../readme.md", std::ios::in | std::ios::binary);
    REQUIRE_THROWS(BMP::Read(input1));

    std::ifstream input2("../examples/example.bmp", std::ios::in | std::ios::binary);
    input2.close();
    REQUIRE_THROWS(BMP::Read(input2));

    std::ofstream output1("../examples/output.bmp", std::ios::out | std::ios::binary);
    output1.close();
    REQUIRE_THROWS(BMP::Write(output1, Image(1, 1)));
}