#pragma once

#include "image.h"

namespace BMP {
    Image Read(std::ifstream &input);

    void Write(std::ofstream &output, const Image &img);
}
