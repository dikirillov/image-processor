#pragma once

#include "matrix_filter.h"

class Sharpening : public Filter {
public:
    Image Apply(const Image &img);
};
