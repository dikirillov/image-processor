#include "edge_detection.h"


EdgeDetectionFilter::EdgeDetectionFilter(const int32_t threshold) {
    threshold_ = threshold;
}

static MatrixFilter matrix_edges({Neighbor{0, 0, 4},
                                  Neighbor{1, 0, -1},
                                  Neighbor{-1, 0, -1},
                                  Neighbor{0, 1, -1},
                                  Neighbor{0, -1, -1}});

Image EdgeDetectionFilter::Apply(const Image &img) {
    int32_t width = img.GetWidth();
    int32_t height = img.GetHeight();
    auto f = std::make_unique<GrayScale>();
    Image gray = f->Apply(img);
    Image ans(width, height);
    ans = matrix_edges.Apply(gray);
    Pixel black_pix{0, 0, 0};
    Pixel white_pix{255, 255, 255};
    for (int32_t y = 0; y < height; ++y) {
        for (int32_t x = 0; x < width; ++x) {
            if (ans.GetOnePixel(x, y).red > threshold_) {
                ans.WriteOnePixel(x, y, white_pix);
            } else {
                ans.WriteOnePixel(x, y, black_pix);
            }
        }
    }
    return ans;
}
