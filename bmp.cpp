#include "bmp.h"

template<typename T>
void ReadInputValue(std::ifstream &input, T &value) {
    input.read(reinterpret_cast<char *>(&value), sizeof(value));
}

template<typename T>
void WriteOutputValue(std::ofstream &output, T value) {
    output.write(reinterpret_cast<char *>(&value), sizeof(value));
}

void WriteZeros(std::ofstream &output, const int value) {
    for (int i = 0; i < value; ++i) {
        output.write("\0", 1);
    }
}

int32_t GetPadding(const int32_t &width) {
    return (4 - 3 * width % 4) % 4;
}

namespace Consts {
    const int16_t bmp_format = 0x4D42;
    const int16_t numb_of_colors = 1;
    const int16_t numb_of_bits = 24;
    const int32_t metre_horizon = 2835;
    const int32_t metre_vertical = 2835;
    const int32_t header_size = 14;
    const int32_t minimal_headers_size = 54;
    const int32_t offset = 54;
    const int skip_to_offset = 8;
    const int skip_to_width = 4;
};

Image BMP::Read(std::ifstream &input) {
    if (input.is_open()) {
        int16_t file_type;
        ReadInputValue(input, file_type);
        if (file_type != Consts::bmp_format) {
            throw std::invalid_argument("incorrect input file type");
        }
        input.seekg(Consts::skip_to_offset, std::ios::cur);
        int32_t offset;
        int32_t width;
        int32_t height;
        ReadInputValue(input, offset);
        input.seekg(Consts::skip_to_width, std::ios::cur);
        ReadInputValue(input, width);
        ReadInputValue(input, height);
        input.seekg(offset, std::ios::beg);
        Image ans(width, abs(height));
        const int padding = GetPadding(width);
        for (int32_t y = 0; y < height; ++y) {
            for (int32_t x = 0; x < width; ++x) {
                Pixel current_pix;
                ReadInputValue(input, current_pix.blue);
                ReadInputValue(input, current_pix.green);
                ReadInputValue(input, current_pix.red);
                if (height > 0) {
                    ans.WriteOnePixel(x, height - y - 1, current_pix);
                } else {
                    ans.WriteOnePixel(x, y, current_pix);
                }
            }
            input.seekg(padding, std::ios::cur);
        }
        input.close();
        return ans;
    }
    throw std::invalid_argument("closed input file");
}

void BMP::Write(std::ofstream &output, const Image &img) {
    if (output.is_open()) {
        const int32_t width = img.GetWidth();
        const int32_t height = img.GetHeight();
        const int padding = GetPadding(width);
        output.write("BM", 2);
        const int32_t size = Consts::offset + height * (3 * width + padding);
        WriteOutputValue(output, size);
        WriteZeros(output, 4);
        WriteOutputValue(output, Consts::offset);

        const int32_t length_till_end = Consts::offset - Consts::header_size;
        WriteOutputValue(output, length_till_end);
        WriteOutputValue(output, width);
        WriteOutputValue(output, height);
        WriteOutputValue(output, Consts::numb_of_colors);
        WriteOutputValue(output, Consts::numb_of_bits);
        WriteZeros(output, 4);
        const int32_t board = (width + padding) * height;
        WriteOutputValue(output, board);
        WriteOutputValue(output, Consts::metre_horizon);
        WriteOutputValue(output, Consts::metre_vertical);
        WriteZeros(output, 4);
        WriteZeros(output, 4);
        for (int i = 0; i < Consts::offset - Consts::minimal_headers_size; ++i) {
            output.write("\0", 1);
        }
        for (int32_t y = height - 1; y > -1; --y) {
            for (int32_t x = 0; x < width; ++x) {
                Pixel current_pixel = img.GetOnePixel(x, y);
                WriteOutputValue(output, current_pixel.blue);
                WriteOutputValue(output, current_pixel.green);
                WriteOutputValue(output, current_pixel.red);
            }
            WriteZeros(output, padding);
        }
        output.close();
    } else {
        throw std::invalid_argument("closed output file");
    }
}
