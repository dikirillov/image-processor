#pragma once

#include "filters.h"

class Crop : public Filter {
public:
    Crop(const int32_t width, const int32_t height);

    Image Apply(const Image &img);

private:
    int32_t width_;
    int32_t height_;
};
