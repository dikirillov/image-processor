#include "bmp.h"
#include "parser.h"
#include "image.h"
#include <iostream>
#include <string>
#include <iterator>

int main(int argc, const char *argv[]) {
    if (argc <= 2) {
        std::cout
                << "This is image processor. You should use input and output file in bmp format." << std::endl
                << "You can use such filters as:" << std::endl
                << "crop (-crop) with 2 parameters" << std::endl
                << "negative (-neg)" << std::endl
                << "grayscale (-gs)" << std::endl
                << "sharpening (-sharp)" << std::endl
                << "edge detection (-edge) with 2 parameters" << std::endl
                << "gaussian blur (-blur) with 1 parameter" << std::endl
                << "pixel filter (-pixel)" << std::endl;
        return 0;
    }
    std::ifstream input(argv[1], std::ios::in | std::ios::binary);
    Image img = BMP::Read(input);
    Image ans_image = img;
    Parser::Parser(ans_image, argc, argv);
    std::ofstream output(argv[2], std::ios::out | std::ios::binary);
    BMP::Write(output, ans_image);
    std::cout << "end of program" << std::endl;
    return 0;
}
