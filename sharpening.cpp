#include "sharpening.h"

static MatrixFilter matrix_edge({{0,  0,  5},
                                 {1,  0,  -1},
                                 {-1, 0,  -1},
                                 {0,  1,  -1},
                                 {0,  -1, -1}});

Image Sharpening::Apply(const Image &img) {
    return matrix_edge.Apply(img);
}
