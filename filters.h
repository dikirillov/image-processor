#pragma once

#include "bmp.h"

class Filter {
public:
    virtual Image Apply(const Image &img) = 0;
};
