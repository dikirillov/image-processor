#include "gray_scale.h"

Image GrayScale::Apply(const Image &img) {
    int32_t width = img.GetWidth();
    int32_t height = img.GetHeight();
    Image ans(width, height);
    for (int32_t y = 0; y < height; ++y) {
        for (int32_t x = 0; x < width; ++x) {
            Pixel current_pixel = img.GetOnePixel(x, y);
            current_pixel.red =
                    0.299 * current_pixel.red + 0.587 * current_pixel.green + 0.114 * current_pixel.blue;
            current_pixel.green = current_pixel.red;
            current_pixel.blue = current_pixel.red;
            ans.WriteOnePixel(x, y, current_pixel);
        }
    }
    return ans;
}