#pragma once

#include "matrix_filter.h"

class GaussianBlur : public Filter {
public:
    GaussianBlur(const int32_t sigma);

    Image Apply(const Image &img);

private:
    int32_t sigma_;
};
