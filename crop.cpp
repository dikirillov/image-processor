#include "crop.h"

Crop::Crop(const int32_t width, const int32_t height) {
    width_ = width;
    height_ = height;
}

Image Crop::Apply(const Image &img) {
    int32_t new_width = std::min(width_, img.GetWidth());
    int32_t new_height = std::min(height_, img.GetHeight());
    Image ans(new_width, new_height);
    for (int32_t y = 0; y < new_height; ++y) {
        for (int32_t x = 0; x < new_width; ++x) {
            ans.WriteOnePixel(x, y, img.GetOnePixel(x, y));
        }
    }
    return ans;
}
