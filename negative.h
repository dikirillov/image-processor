#pragma once

#include "filters.h"

class Negative : public Filter {
public:
    Image Apply(const Image &img);
};
