#include "bmp.h"
#include "crop.h"
#include "edge_detection.h"
#include "gaussian_blur.h"
#include "gray_scale.h"
#include "negative.h"
#include "pixel_filter.h"
#include "sharpening.h"
#include "image.h"
#include <iostream>
#include <string>
#include <iterator>
#include <vector>

namespace Parser {
    void Parser(Image &img, int argc, const char *argv[]);
}
