#pragma once

#include <iostream>
#include <fstream>
#include <memory>
#include <vector>

struct Pixel {
    uint8_t red = 0;
    uint8_t green = 0;
    uint8_t blue = 0;
};

class Image {
public:
    Image(int32_t width, int32_t height);

    void WriteOnePixel(const int32_t x, const int32_t y, Pixel pix);

    Pixel GetOnePixel(const int32_t x, const int32_t y) const;

    int32_t GetWidth() const;

    int32_t GetHeight() const;

    Image& operator=(const Image &other);

    bool operator==(const Image &other) const;

private:
    std::vector<std::vector<Pixel>> data_;
    int32_t width_ = 0;
    int32_t height_ = 0;
};
